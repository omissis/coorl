<?php

/*
 * This file is part of the Coorl package.
 *
 * (c) Claudio Beatrice <claudi0.beatric3@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * This class produces cURL handles
 *
 * @package    Coorl
 * @subpackage Http
 * @author     Claudio Beatrice <claudi0.beatric3@gmail.com>
 */

namespace Coorl\Handle;

use Coorl\Handle\Single as SingleHandle;
use Coorl\Exception as CoorlException;

class Factory
{
    /**
     * Returns an array with a set of default options for cURL.
     *
     * @return array
     */
    public static function getDefaultOptions()
    {
        return array(
            CURLOPT_HEADER => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 10,
        );
    }

    /**
     * Creates and initializes a single cURL handle.
     *
     * @return Coorl\Handle\Single
     */
    public static function createSingleHandle(array $options = array())
    {
        $defaultOptions = self::getDefaultOptions();

        return new SingleHandle(null, $defaultOptions + $options);
    }

    /**
     * Creates and initializes a multi cURL handle.
     *
     * * @return Coorl\Handle\Single
     */
    public static function createMultiHandle(array $handles = array())
    {
        return new MultiHandle($handles);
    }
}
