<?php

/*
 * This file is part of the Coorl package.
 *
 * (c) Claudio Beatrice <claudi0.beatric3@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * This class is a wrapper for a cURL multi handle.
 *
 * @package    Coorl
 * @subpackage Http
 * @author     Claudio Beatrice <claudi0.beatric3@gmail.com>
 */

namespace Coorl\Handle;

class Multi
{
    protected $handle;

    public function __construct(array $handles = array())
    {
        $this->init();

        foreach ($handles as $handle) {
            $this->addHandle($handle);
        }
    }

    public function __destruct()
    {
        $this->close();
    }

    /**
     * Add a normal cURL handle to a cURL multi handle.
     *
     * @link http://www.php.net/manual/en/function.curl-multi-add-handle.php
     *
     * @param resource $singleHandle
     *   A cURL handle returned by curl_init().
     *
     * @return int
     *   Returns 0 on success, or one of the CURLM_XXX errors code.
     */
    public function addHandle($singleHandle)
    {
        return curl_multi_add_handle($this->handle, $singleHandle);
    }

    /**
     * Close a set of cURL handles.
     *
     * @link http://www.php.net/manual/en/function.curl-multi-close.php
     */
    public function close()
    {
        return curl_multi_close($this->handle);
    }

    /**
     * Run the sub-connections of the current cURL handle.
     *
     * @link http://www.php.net/manual/en/function.curl-multi-exec.php
     *
     * @param int $stillRunning
     *   A reference to a flag to tell whether the operations are still running.
     *
     * @return int
     *   A cURL code defined in the cURL Predefined Constants.
     */
    public function exec(&$stillRunning)
    {
        return curl_multi_exec($this->handle, $stillRunning);
    }

    /**
     * Return the content of a cURL handle if CURLOPT_RETURNTRANSFER is set.
     *
     * @link http://www.php.net/manual/en/function.curl-multi-getcontent.php
     *
     * @param resource $singleHandle
     *   A cURL handle returned by curl_init().
     *
     * @return string
     *   If CURLOPT_RETURNTRANSFER is an option that is set for a specific handle,
     *   then this function will return the content of that cURL handle in the form of a string.
     */
    public function getContent($singleHandle)
    {
        return curl_multi_getcontent($singleHandle);
    }

    /**
     * Get information about the current transfers.
     *
     * @link http://www.php.net/manual/en/function.curl-multi-info-read.php
     *
     * @param int $messagesInQueue
     *   Number of messages that are still in the queue.
     *
     * @return array
     *   On success, returns an associative array for the message, FALSE on failure.
     */
    public function infoRead($messagesInQueue = null)
    {
        return curl_multi_info_read($this->handle, $messagesInQueue);
    }

    /**
     * Allow the processing of multiple cURL handles in parallel.
     *
     * @link http://www.php.net/manual/en/function.curl-multi-init.php
     *
     * @return resource
     *   Returns a cURL multi handle resource on success, FALSE on failure.
     */
    public function init()
    {
        $this->handle = curl_multi_init();

        return $this->handle;
    }

    /**
     * Remove a multi handle from a set of cURL handles.
     *
     * @link http://www.php.net/manual/en/function.curl-multi-remove handle.php
     *
     * @param resource $singleHandle
     *   A cURL handle returned by curl_init().
     *
     * @return int
     *   Returns 0 on success, or one of the CURLM_XXX error codes.
     */
    public function removeHandle($singleHandle)
    {
        return curl_multi_remove_handle($this->handle, $singleHandle);
    }
}
