<?php

/*
 * This file is part of the Coorl package.
 *
 * (c) Claudio Beatrice <claudi0.beatric3@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * This class is a wrapper for a cURL single handle.
 *
 * @package    Coorl
 * @subpackage Http
 * @author     Claudio Beatrice <claudi0.beatric3@gmail.com>
 */

namespace Coorl\Handle;

class Single
{
    protected $handle;

    public function __construct($url = null, array $options = array())
    {
        $this->init($url);
        $this->setOptionsArray($options);
    }

    public function __destruct()
    {
        $this->close();
    }

    public function getResource()
    {
        return $this->handle;
    }

    /**
     * Close the underlying cURL session.
     *
     * @link http://www.php.net/manual/en/function.curl-close.php
     *
     * @return void
     */
    public function close()
    {
        if (is_resource($this->handle)) {
            curl_close($this->handle);
        }
    }

    /**
     * Copy the underlying cURL handle along with all of its preferences.
     *
     * @link http://www.php.net/manual/en/function.curl-copy-handle.php
     *
     * @return resource
     *   a new cURL handle.
     */
    public function copyHandle()
    {
        return curl_copy_handle($this->handle);
    }

    /**
     * Return the last error number.
     *
     * @link http://www.php.net/manual/en/function.curl-errno.php
     *
     * @return int
     *   the error number or 0 (zero) if no error occurred.
     */
    public function errno()
    {
        return curl_errno($this->handle);
    }

    /**
     * Return a string containing the last error for the current session.
     *
     * @link http://www.php.net/manual/en/function.curl-error.php
     *
     * @return string
     *   the error message or '' (the empty string) if no error occurred.
     */
    public function error()
    {
        return curl_error($this->handle);
    }

    /**
     * Perform a cURL session.
     *
     * @link http://www.php.net/manual/en/function.curl-exec.php
     *
     * @return mixed
     *   Returns TRUE on success or FALSE on failure.
     *   However, if the CURLOPT_RETURNTRANSFER option is set,
     *   it will return the result on success, FALSE on failure.
     */
    public function exec()
    {
        return curl_exec($this->handle);
    }

    /**
     * Get information regarding a specific transfer.
     *
     * @link http://www.php.net/manual/en/function.curl-getinfo.php
     *
     * @param int $option
     *
     * @return mixed
     *   If opt is given, returns its value as a string.
     *   Otherwise, returns an associative array.
     */
    public function getinfo($option = 0)
    {
        return curl_getinfo($this->handle, $option);
    }

    /**
     * Initialize a cURL session.
     *
     * @link http://www.php.net/manual/en/function.curl-init.php
     *
     * @param string $url
     *
     * @return mixed
     *   Returns a cURL handle on success, FALSE on errors.
     */
    public function init($url = null)
    {
        $this->handle = curl_init($url);

        return $this->handle;
    }

    /**
     * Set multiple options for a cURL transfer.
     *
     * @link http://www.php.net/manual/en/function.curl-setopt-array.php
     *
     * @param array $options
     *
     * @return boolean
     *   Returns TRUE if all options were successfully set.
     *   If an option could not be successfully set, FALSE is immediately returned,
     *   ignoring any future options in the options array.
     */
    public function setOptionsArray(array $options)
    {
        if (!empty($options)) {
            $this->options = $options;

            return curl_setopt_array($this->handle, $options);
        }

        return true;
    }

    /**
     * Set an option for a cURL transfer.
     *
     * @link http://www.php.net/manual/en/function.curl-setopt.php
     *
     * @param int $option
     *
     * @param mixed $value
     *
     * @return boolean
     *   TRUE on success or FALSE on failure.
     */
    public function setOption($option, $value)
    {
        $this->options[$option] = $value;

        return curl_setopt($this->handle, $option, $value);
    }

    /**
     * Get the options for a cURL transfer.
     *
     * @see setOption()
     * @see setOptionsArray()
     * @see addOptionsArray()
     *
     * @return array
     *   The options for the a cURL transfer.
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Add multiple options for a cURL transfer.
     *
     * @see setOptionsArray()
     *
     * @param array $options
     *
     * @return boolean
     *   Returns TRUE if all options were successfully set.
     *   If an option could not be successfully set, FALSE is immediately returned,
     *   ignoring any future options in the options array.
     */
    public function addOptionsArray(array $options)
    {
        if (!empty($options)) {
            foreach ($options as $key => $value) {
                $this->options[$key] = $value;
            }

            return curl_setopt_array($this->handle, $this->options);
        }

        return true;
    }

    /**
     * Get cURL version information.
     *
     * @link http://www.php.net/manual/en/function.curl-version.php
     *
     * @param int $age
     *
     * @return array
     *   Returns an associative array.
     */
    public function version($age = CURLVERSION_NOW)
    {
        return curl_version($age);
    }
}
