<?php

/*
 * This file is part of the Coorl package.
 *
 * (c) Claudio Beatrice <claudi0.beatric3@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * This class represents a simple cURL client.
 *
 * @package    Coorl
 * @author     Alessandro Nadalin <alessandro.nadalin@gmail.com>
 * @author     Daniele Alessandri <suppakilla@gmail.com>
 * @author     Claudio Beatrice <claudi0.beatric3@gmail.com>
 */

namespace Coorl;

use Coorl\Handle\Factory as HandleFactory;
use Coorl\Exception as CoorlException;

class Client
{
    private $handle;
    private $restart;

    /**
     * Create a new Client based in cURL.
     *
     * @param boolean $restart
     * @param Coorl\Handle\Single $handle
     */
    public function __construct($restart = false, $handle = null)
    {
        $this->handle = $handle ?: HandleFactory::createSingleHandle();

        $this->restart = $restart;
    }

    /**
     * Close the underlying cURL handle.
     */
    public function __destruct()
    {
        unset($this->handle);
    }

    /**
     * Return the underlying Coorl handle.
     *
     * @return Coorl\Handle\Single
     */
    public function getHandle()
    {
        return $this->handle;
    }

    /**
     * Reinitialize the client for a completely new session.
     */
    public function restart($handle = null)
    {
        if (isset($this->handle)) {
            unset($this->handle);
        }

        $this->handle = $handle ?: HandleFactory::createSingleHandle();
    }

    /**
     * Execute a request.
     *
     * @param array $options
     *
     * @return string $response
     */
    public function execute(array $options = array())
    {
        $this->handle->addOptionsArray($options);

        if (!$response = $this->getHandle()->exec()) {
            throw new CoorlException('Empty response');
        }

        if ($this->restart === true) {
            $this->restart();
        }

        return $response;
    }
}
