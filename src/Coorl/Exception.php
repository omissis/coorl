<?php

/*
 * This file is part of the Coorl package.
 *
 * (c) Claudio Beatrice <claudi0.beatric3@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * A base Coorl exception.
 *
 * @package    Coorl
 * @author     Claudio Beatrice <claudi0.beatric3@gmail.com>
 */

namespace Coorl;

class Exception extends \Exception
{
}
