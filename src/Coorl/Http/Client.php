<?php

/*
 * This file is part of the Coorl package.
 *
 * (c) Claudio Beatrice <claudi0.beatric3@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * This class represents an HTTP client based on Curl.
 *
 * @package    Coorl
 * @subpackage Http
 * @author     Alessandro Nadalin <alessandro.nadalin@gmail.com>
 * @author     Daniele Alessandri <suppakilla@gmail.com>
 * @author     Claudio Beatrice <claudi0.beatric3@gmail.com>
 */

namespace Coorl\Http;

use Coorl\Client as CoorlClient;
use Coorl\Exception as CoorlException;
use Coorl\Http\ClientInterface as HttpClientInterface;

class Client extends CoorlClient implements HttpClientInterface
{
    private $cookies = array();

    /**
     * @{inheritdoc}
     *
     * @throws CoorlException
     *   Not Implemented Yet
     */
    public function connect()
    {
        throw new CoorlException("Not Implemented Yet");
    }

    /**
     * @{inheritdoc}
     */
    public function delete($location, $body = null)
    {
        $request = new ClientRequest(
            array(
                CURLOPT_CUSTOMREQUEST => 'DELETE',
                CURLOPT_URL => $location,
                CURLOPT_POSTFIELDS => $body,
            ),
            $this->cookies
        );

        return $this->execute($request->getOptions());
    }

    /**
     * @{inheritdoc}
     */
    public function get($location)
    {
        $request = new ClientRequest(
            array(
                CURLOPT_HTTPGET => true,
                CURLOPT_URL => $location,
            ),
            $this->cookies
        );

        return $this->execute($request->getOptions());
    }

    /**
     * @{inheritdoc}
     *
     * @throws CoorlException
     *   Not Implemented Yet
     */
    public function head()
    {
        throw new CoorlException("Not Implemented Yet");
    }

    /**
     * @{inheritdoc}
     *
     * @throws CoorlException
     *   Not Implemented Yet
     */
    public function options()
    {
        throw new CoorlException("Not Implemented Yet");
    }

    /**
     * @{inheritdoc}
     *
     * @throws CoorlException
     *   Not Implemented Yet
     */
    public function patch()
    {
        throw new CoorlException("Not Implemented Yet");
    }

    /**
     * @{inheritdoc}
     */
    public function post($location, $body)
    {
        $request = new ClientRequest(
            array(
                CURLOPT_POST => true,
                CURLOPT_URL => $location,
                CURLOPT_POSTFIELDS => $body,
            ),
            $this->cookies
        );

        return $this->execute($request->getOptions());
    }

    /**
     * @{inheritdoc}
     */
    public function put($location, $body)
    {
        $request = new ClientRequest(
            array(
                CURLOPT_CUSTOMREQUEST => 'PUT',
                CURLOPT_URL => $location,
                CURLOPT_POSTFIELDS => $body,
            ),
            $this->cookies
        );

        return $this->execute($request->getOptions());
    }

    /**
     * @{inheritdoc}
     *
     * @throws CoorlException
     *   Not Implemented Yet
     */
    public function trace()
    {
        throw new CoorlException("Not Implemented Yet");
    }

    /**
     * @{inheritdoc}
     */
    public function getSupportedMethods()
    {
        return array('connect', 'delete', 'get', 'head', 'options', 'patch', 'post', 'put', 'trace');
    }

    /**
     * @{inheritdoc}
     */
    public function execute(array $options = array())
    {
        try {
            $response = parent::execute($options);

            if (true === $options[ClientInterface::COORLOPT_RAW]) {
                return $response;
            }
        } catch (CoorlException $e) {
            throw new EmptyResponseException($this, $options[CURLOPT_URL]);
        }

        $response = new ClientResponse($response);

        $this->cookies = array_merge($this->cookies, $response->getCookies());

        return $response;
    }

    /**
     * @{inheritdoc}
     */
    public function restart($handle = null)
    {
        $this->cookies = array();

        parent::restart($handle);
    }
}
