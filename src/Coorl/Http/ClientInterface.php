<?php

/*
 * This file is part of the Coorl package.
 *
 * (c) Claudio Beatrice <claudi0.beatric3@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * This interface represents the public contract for Coorl Http Client.
 *
 * @package    Coorl
 * @subpackage Http
 * @author     Claudio Beatrice <claudi0.beatric3@gmail.com>
 */

namespace Coorl\Http;

interface ClientInterface
{
    const COORLOPT_RAW = 5000000;

    /**
     * Perform an HTTP CONNECT request.
     */
    public function connect();

    /**
     * Perform an HTTP DELETE request.
     *
     * @param  String $location
     *
     * @return ClientResponse
     */
    public function delete($location, $body = null);

    /**
     * Perform an HTTP GET request.
     *
     * @param   String $location
     *
     * @return  ClientResponse
     */
    public function get($location);

    /**
     * Perform an HTTP HEAD request.
     *
     * @return ClientResponse
     */
    public function head();

    /**
     * Perform an HTTP OPTIONS request.
     *
     * @return ClientResponse
     */
    public function options();

    /**
     * Perform an HTTP PATCH request.
     *
     * @return ClientResponse
     */
    public function patch();

    /**
     * Perform an HTTP POST request.
     *
     * @param   String $location
     * @param   String $body
     *
     * @return  ClientResponse
     */
    public function post($location, $body);

    /**
     * Perform an HTTP PUT request.
     *
     * @param   String $location
     * @param   String $body
     *
     * @return  ClientResponse
     */
    public function put($location, $body);

    /**
     * Perform an HTTP TRACE request.
     *
     * @return ClientResponse
     */
    public function trace();

    /**
     * Return supported HTTP Methods.
     *
     * @return array
     */
    public function getSupportedMethods();
}
