<?php

/*
 * This file is part of the Coorl package.
 *
 * (c) Claudio Beatrice <claudi0.beatric3@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * This class wraps an HTTP request made by the Curl client.
 *
 * @package    Coorl
 * @subpackage Http
 * @author     Claudio Beatrice <claudi0.beatric3@gmail.com>
 */

namespace Coorl\Http;

use Coorl\Exception as CoorlException;

class ClientRequest
{
    private $options;

    public function __construct(array $options = array(), array $cookies = array())
    {
        if (!isset($options[CURLOPT_URL]) || empty($options[CURLOPT_URL])) {
            throw new CoorlException("CURLOPT_URL is mandatory");
        }

        $defaultOptions = array(
            CURLOPT_COOKIE => $this->formatCookies($cookies),
            CURLOPT_RETURNTRANSFER => true,
            ClientInterface::COORLOPT_RAW => false,
        );

        foreach ($defaultOptions as $key => $value) {
            if (!isset($options[$key])) {
                $options[$key] = $value;
            }
        }

        $this->options = $options;
    }

    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Return a string with the list of cookies for the Cookie header.
     *
     * @return string
     */
    private function formatCookies(array $cookies)
    {
        $pairs = array();

        foreach ($cookies as $k => $v) {
            $pairs[] = "$k=$v";
        }

        return implode($pairs, ';');
    }
}
