<?php

namespace Coorl\Tests\Http;

use Coorl\Http\ClientRequest;
use Coorl\Http\ClientInterface;

class ClientRequestTest extends \PHPUnit_Framework_TestCase
{
    public function testGetOptions()
    {
        $request = new ClientRequest(
            array(CURLOPT_URL => 'http://foo.bar/'),
            array('foo' => 'bar', 'baz' => 'quux')
        );

        $this->assertEquals(
            array(
                CURLOPT_COOKIE => 'foo=bar;baz=quux',
                CURLOPT_RETURNTRANSFER => true,
                ClientInterface::COORLOPT_RAW => false,
                CURLOPT_URL => 'http://foo.bar/'
            ),
            $request->getOptions()
        );
    }

    /**
     * @expectedException Coorl\Exception
     * @expectedExceptionMessage CURLOPT_URL is mandatory
     */
    public function testMissingMandatoryURLParameter()
    {
        new ClientRequest(
            array('foo' => 'bar', 'baz' => 'quux')
        );
    }
}
