<?php

namespace Coorl\Tests\Http;

use Coorl\Http\ClientResponse;
use Coorl\Http\ClientInterface;

class ClientResponseTest extends \PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $this->responseBody =
            'HTTP/1.1 200 OK' . "\r\n" .
            'Date: Tue, 25 Dec 1990 13:37:00 GMT' . "\r\n" .
            'Content-Type: text/html; charset=UTF-8' . "\r\n" .
            'Set-Cookie: logged_in=no; domain=.f.oo; path=/; expires=Mon, 02-May-2033 16:13:45 GMT; HttpOnly' . "\r\n" .
            'Set-Cookie: _foo_sess=1234; path=/; expires=Sun, 01-Jan-2023 00:00:00 GMT; secure; HttpOnly' . "\r\n\r\n" .
            'foo bar';
        $this->response = new ClientResponse($this->responseBody);
    }

    public function testGetBody()
    {
        $this->assertSame('foo bar', $this->response->getBody());

        $responseBody = 'foo bar';
        $response = new ClientResponse($responseBody);

        $this->assertSame($responseBody, $response->getBody());
    }

    public function testGetHeader()
    {
        $this->assertSame('Tue, 25 Dec 1990 13:37:00 GMT', $this->response->getHeader('Date'));
        $this->assertSame('text/html; charset=UTF-8', $this->response->getHeader('Content-Type'));
    }

    public function testGetCookies()
    {
        $cookies = $this->response->getCookies();

        $this->assertInternalType('array', $cookies);
        $this->assertNotEmpty($cookies);
        $this->assertSame('no', $cookies['logged_in']);
        $this->assertSame('1234', $cookies['_foo_sess']);
    }

    public function testGetResponse()
    {
        $this->assertSame($this->responseBody, $this->response->getResponse());
        $this->assertSame($this->responseBody, (string)$this->response);
    }

    public function testGetValidStatusCodes()
    {
        $this->assertEquals(
            array(200, 201, 202, 203, 204, 205, 206),
            $this->response->getValidStatusCodes()
        );
    }

    public function testGetProtocol()
    {
        $this->assertSame('HTTP/1.1', $this->response->getProtocol());
    }

    public function testGetStatusCode()
    {
        $this->assertSame(200, $this->response->getStatusCode());
    }
}
