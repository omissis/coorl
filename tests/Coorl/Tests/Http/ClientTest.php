<?php

namespace Coorl\Tests\Http;

use Coorl\Http\Client;
use Coorl\Http\ClientInterface;
use Coorl\Http\EmptyResponseException;
use Coorl\Handle\Factory as HandleFactory;

class ClientTest extends \PHPUnit_Framework_TestCase
{
    protected $handle;
    protected $client;

    protected function setUp()
    {
        $this->handle = $this->getMock('\\Coorl\\Handle\\Single');

        $this->handle
            ->expects($this->any())
            ->method('exec')
            ->will(
                $this->returnValue(
                    'HTTP/1.1 200 OK' . "\r\n" .
                    'Date: Tue, 25 Dec 1990 13:37:00 GMT' . "\r\n" .
                    'Content-Type: text/html; charset=UTF-8' . "\r\n\r\n" .
                    'foo bar'
                )
            );

        $this->client = new Client(false, $this->handle);
    }

    /**
     * @todo this looks more like an integration test, so probably I should move it to a dedicated location
     */
    public function testExecuteOptions()
    {
        $client = new Client();
        $defaultOptions = HandleFactory::getDefaultOptions();
        $additionalOptions = array(
            CURLOPT_URL => '127.0.0.1',
            ClientInterface::COORLOPT_RAW => true,
        );

        $client->execute($additionalOptions);

        $this->assertEquals($defaultOptions + $additionalOptions, $client->getHandle()->getOptions());
    }

    public function testRawResponse()
    {
        $body = 'foo=bar&baz=quux';

        $this->handle = $this->getMock('\\Coorl\\Handle\\Single');

        $this->handle
            ->expects($this->exactly(1))
            ->method('exec')
            ->will($this->returnValue($body));

        $this->client = new Client(false, $this->handle);

        $this->assertSame(
            $body,
            $this->client->execute(
                array(
                    CURLOPT_URL => 'http://foo.bar/',
                    ClientInterface::COORLOPT_RAW => true,
                )
            )
        );
    }

    public function testEmptyResponse()
    {
        $this->handle = $this->getMock('\\Coorl\\Handle\\Single');

        $this->handle
            ->expects($this->exactly(1))
            ->method('exec')
            ->will($this->returnValue(''));

        $this->client = new Client(false, $this->handle);

        try {
            $this->client->execute(array(CURLOPT_URL => 'http://foo.bar/'));
        } catch (EmptyResponseException $ere) {
            $this->assertSame(
                'Coorl\Http\Client has been unable to retrieve a response for the resource at http://foo.bar/',
                $ere->getMessage()
            );

            $this->assertSame($this->client, $ere->getClient());
            $this->assertSame('http://foo.bar/', $ere->getLocation());
            return;
        }

        $this->fail('An expected Coorl\Http\EmptyResponseException has not been raised.');
    }

    /**
     * @expectedException        Coorl\Exception
     * @expectedExceptionMessage Not Implemented Yet
     */
    public function testConnect()
    {
        $this->client->connect();
    }

    public function testDelete()
    {
        $response = $this->client->delete('http://foo.bar/', 'Foo Bar Baz Quux');

        $this->responseTestHelper($response, 'foo bar');
    }

    public function testGet()
    {
        $response = $this->client->get('http://foo.bar/');

        $this->responseTestHelper($response, 'foo bar');
    }

    /**
     * @expectedException        Coorl\Exception
     * @expectedExceptionMessage Not Implemented Yet
     */
    public function testHead()
    {
        $this->client->head();
    }

    /**
     * @expectedException        Coorl\Exception
     * @expectedExceptionMessage Not Implemented Yet
     */
    public function testOptions()
    {
        $this->client->options();
    }

    /**
     * @expectedException        Coorl\Exception
     * @expectedExceptionMessage Not Implemented Yet
     */
    public function testPatch()
    {
        $this->client->patch();
    }

    public function testPost()
    {
        $response = $this->client->post('http://foo.bar/', 'foo=bar');

        $this->responseTestHelper($response, 'foo bar');
    }

    public function testPut()
    {
        $response = $this->client->put('http://foo.bar/', 'foo=bar');

        $this->responseTestHelper($response, 'foo bar');
    }

    /**
     * @expectedException        Coorl\Exception
     * @expectedExceptionMessage Not Implemented Yet
     */
    public function testTrace()
    {
        $this->client->trace();
    }

    private function responseTestHelper($response, $body, $cookies = array(), $statusCode = 200, $protocol = 'HTTP/1.1')
    {
        $this->assertInstanceOf('Coorl\\Http\\ClientResponse', $response);

        $this->assertSame($statusCode, $response->getStatusCode());
        $this->assertSame($protocol, $response->getProtocol());
        $this->assertSame($cookies, $response->getCookies());
        $this->assertSame($body, $response->getBody());
    }
}
