<?php

namespace Coorl\Tests\Handle;

use Coorl\Handle\Single as SingleHandle;

class SingleTest extends \PHPUnit_Framework_TestCase
{
    public function testSetOptions()
    {
        $handle = new SingleHandle();
        $defaultOptions = array(
            CURLOPT_HEADER => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 10,
        );
        $additionalOptions = array(
            CURLOPT_HTTPGET => true,
            CURLOPT_URL => 'http://foo.bar/',
        );

        $handle->setOptionsArray($defaultOptions);

        $this->assertEquals($defaultOptions, $handle->getOptions());

        $handle->setOptionsArray($additionalOptions);

        $this->assertEquals($additionalOptions, $handle->getOptions());
    }

    public function testAddOptions()
    {
        $handle = new SingleHandle();
        $defaultOptions = array(
            CURLOPT_HEADER => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 10,
        );
        $additionalOptions = array(
            CURLOPT_HTTPGET => true,
            CURLOPT_URL => 'http://foo.bar/',
        );

        $handle->setOptionsArray($defaultOptions);

        $this->assertEquals($defaultOptions, $handle->getOptions());

        $handle->addOptionsArray($additionalOptions);

        $this->assertEquals($defaultOptions + $additionalOptions, $handle->getOptions());
    }
}
