<?php

namespace Coorl\Tests;

use Coorl\Client;

class ClientTest extends \PHPUnit_Framework_TestCase
{
    public function testDefaultInit()
    {
        $client = new Client();

        $this->assertInstanceOf('\\Coorl\\Handle\\Single', $client->getHandle());
    }

    /**
     * @expectedException Coorl\Exception
     * @expectedExceptionMessage Empty response
     */
    public function testExecuteFail()
    {

        $handle = $this->getMock('\\Coorl\\Handle\\Single');

        $handle
            ->expects($this->exactly(1))
            ->method('exec')
            ->will($this->returnValue(''));

        $client = new Client(false, $handle);

        $client->execute(
            array(
                CURLOPT_HTTPGET => true,
                CURLOPT_URL => 'http://foo.bar/',
            )
        );
    }

    public function testGetRequestSuccess()
    {
        $handle = $this->getMock('\\Coorl\\Handle\\Single');

        $handle
            ->expects($this->exactly(1))
            ->method('exec')
            ->will($this->returnValue('foo bar'));

        $client = new Client(false, $handle);

        $response = $client->execute(
            array(
                CURLOPT_HTTPGET => true,
                CURLOPT_URL => 'http://foo.bar/',
            )
        );

        $this->assertEquals('foo bar', $response);
        $this->assertSame($handle, $client->getHandle());
    }

    public function testGetRequestSuccessRestart()
    {
        $handle = $this->getMock('\\Coorl\\Handle\\Single');

        $handle
            ->expects($this->exactly(1))
            ->method('exec')
            ->will($this->returnValue('foo bar'));

        $client = new Client(true, $handle);

        $response = $client->execute(
            array(
                CURLOPT_HTTPGET => true,
                CURLOPT_URL => 'http://foo.bar/',
            )
        );

        $this->assertEquals('foo bar', $response);
        $this->assertNotSame($handle, $client->getHandle());
    }
}
